import { cilChartPie, cilMoodGood, cilCart, cilFridge } from '@coreui/icons'
import CIcon from '@coreui/icons-react'
import { CCol, CRow, CWidgetStatsC } from '@coreui/react'
import { CChart } from '@coreui/react-chartjs'
import React from 'react'
import { AppContent, AppSidebar, AppFooter, AppHeader } from '../components/index'

const DefaultLayout = () => {

  return (
    <div>
      <AppSidebar />
      <div className="wrapper d-flex flex-column min-vh-100 bg-light">
        <AppHeader />
        <div className="body flex-grow-1 px-3">
          <AppContent />
          {/* <h1>Admin Home</h1>
          <CRow>
            <CCol xs={3}>
              <CWidgetStatsC
                className="mb-3"
                progress={{ color: 'primary', value: 100 }}
                text="Customer" 
                title="10"
                value="Customer"
                icon={<CIcon icon={cilMoodGood} height={36} />}
              />
            </CCol>
            <CCol xs={3}>
              <CWidgetStatsC
                className="mb-3"
                icon={<CIcon icon={cilCart} height={36} />}
                progress={{ color: 'warning', value: 100 }}
                text="Widget helper text"
                title="Widget title"
                value="ORDER"
              />
            </CCol>
            <CCol xs={3}>
              <CWidgetStatsC
                className="mb-3"
                icon={<CIcon icon={cilFridge} height={36} />}
                progress={{ color: 'success', value: 100 }}
                text="Widget helper text"
                title="Widget title"
                value="PRODUCT"
              />
            </CCol>

          </CRow>
          <CChart
            type="bar"
            data={{
              labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
              datasets: [
                {
                  label: 'GitHub Commits',
                  backgroundColor: '#f87979',
                  data: [40, 20, 12, 39, 10, 40, 39, 80, 40],
                },
              ],
            }}
            labels="months"
          /> */}
        </div>
        <div style={{ marginTop: "100px" }}>
          <AppFooter />
        </div>
      </div>
    </div>
  )
}

export default DefaultLayout
