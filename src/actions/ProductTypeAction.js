import { ADD_PRODUCT_TYPE, DELETE_PRODUCT_TYPE, EDIT_PRODUCT_TYPE, GET_ALL_PRODUCT_TYPE, PAGINATION } from "src/constants/ProductTypeConst";

export const productTypeACtion = (productName, limit, currentPage_product) => async dispatch => {
    var requestOptions = {
        method: 'GET',
        redirect: 'follow'
    }
    try {
        const response = await fetch(`http://localhost:8000/productTypeRouters?filterName=${productName}&&limit=${limit}&&page=${currentPage_product}`, requestOptions);
        const data = await response.json();
        return dispatch({
            type: GET_ALL_PRODUCT_TYPE,
            payload: {
                total: data.total,
                limit: data.limit,
                productType: data.productType,
            }
        });
    }
    catch (error) {
        console.log(error)
    }
}

export const ChangeNoPage = (value) => {
    return {
        type: PAGINATION,
        payload: value
    }
}

export const addProductTypeAction = (addData, setDisplayAlert, settextAlert, setAlert, setVisible, varRefeshPage, setVarRefeshPage) => async dispatch => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var requestOptions = {
        method: 'POST',
        redirect: 'follow',
        headers: myHeaders,
        body: JSON.stringify(addData)
    }
    try {
        const response = await fetch(`http://localhost:8000/productTypeRouters`, requestOptions);
        const data = await response.json();
        if (data.message) {
            setDisplayAlert(true)
            settextAlert("Add new product type successful")
            setAlert("success")
            setVisible(false)
            setVarRefeshPage(varRefeshPage + 1)
        }
        return dispatch({
            type: ADD_PRODUCT_TYPE,
            payload: data
        });
    }
    catch (error) {
        console.log(error)
    }
}

export const editProductTypeAction = (editData, productId, setDisplayAlert, settextAlert, setAlert, setVisible, varRefeshPage, setVarRefeshPage) => async dispatch => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var requestOptions = {
        method: 'PUT',
        redirect: 'follow',
        headers: myHeaders,
        body: JSON.stringify(editData)
    }
    try {
        const response = await fetch(`http://localhost:8000/productTypeRouters/${productId}`, requestOptions);
        const data = await response.json();
        if (data.message) {
            setDisplayAlert(true)
            settextAlert("Update product type successful")
            setAlert("success")
            setVisible(false)
            setVarRefeshPage(varRefeshPage + 1)
        }
        return dispatch({
            type: EDIT_PRODUCT_TYPE,
            payload: data
        });
    }
    catch (error) {
        console.log(error)
    }
}

export const deleteProductTypeAction = (productId, setDisplayAlert, settextAlert, setAlert, setVisible, varRefeshPage, setVarRefeshPage) => async dispatch => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var requestOptions = {
        method: 'DELETE',
        redirect: 'follow',
        headers: myHeaders,
    }
    try {
        const response = await fetch(`http://localhost:8000/productTypeRouters/${productId}`, requestOptions);
        setDisplayAlert(true)
        settextAlert("Delete product type successful")
        setAlert("success")
        setVisible(false)
        setVarRefeshPage(varRefeshPage + 1)

        return dispatch({
            type: DELETE_PRODUCT_TYPE,
            payload: "success"
        });
    }
    catch (error) {
        console.log(error)
    }
}