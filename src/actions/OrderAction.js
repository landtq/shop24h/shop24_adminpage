import { ADD_ORDER, DELETE_ORDER, EDIT_ORDER, GET_ALL_ORDER, PAGINATION_ORDER } from "src/constants/Ord";

export const orderAction = (orderId) => async dispatch => {
    console.log(orderId)
    var requestOptions = {
        method: 'GET',
        redirect: 'follow'
    }
    try {
        if (orderId) {
            const response = await fetch(`http://localhost:8000/orders`, requestOptions);
            const data = await response.json();
            return dispatch({
                type: GET_ALL_ORDER,
                payload: data.data.filter(item => {
                    return item._id.toLowerCase().includes(orderId.toLowerCase())
                })
            });
        } else {
            const response = await fetch(`http://localhost:8000/orders`, requestOptions);
            const data = await response.json();
            return dispatch({
                type: GET_ALL_ORDER,
                payload: data.data
            });
        }
    }
    catch (error) {
        console.log(error)
    }
}

export const ChangeNoPageOrder = (value) => {
    return {
        type: PAGINATION_ORDER,
        payload: value
    }
}

export const addOrderAction = (addData, cusId, setDisplayAlert, settextAlert, setAlert, setVisible, varRefeshPage, setVarRefeshPage) => async dispatch => {
    var requestOptions = {
        method: 'GET',
        redirect: 'follow'
    }
    try {
        const response = await fetch(`http://localhost:8000/customers`, requestOptions);
        const data = await response.json();
        const result = data?.customer.filter(item =>
            item._id === cusId
        );
        if (result.length = 0 || result == "" || result == []) {
            setDisplayAlert(true)
            settextAlert("Customer is not exist! please check again")
            setVarRefeshPage(varRefeshPage + 1)
        } else {
            var myHeadersOrder = new Headers();
            myHeadersOrder.append("Content-Type", "application/json");
            var requestOptionsOrder = {
                method: 'POST',
                redirect: 'follow',
                body: JSON.stringify(addData),
                headers: myHeadersOrder
            }
            const responseOrder = await fetch(`http://localhost:8000/customers/${cusId}/orders`, requestOptionsOrder);
            const dataOrderResult = await responseOrder.json();
            if (dataOrderResult.status) {
                setDisplayAlert(true)
                settextAlert("Add new order successful")
                setAlert("success")
                setVisible(false)
                setVarRefeshPage(varRefeshPage + 1)
            }
        }
        return dispatch({
            type: ADD_ORDER,
            payload: data
        });
    }
    catch (error) {
        console.log(error)
    }
}

export const editOrderAction = (editData, orderId, setDisplayAlert, settextAlert, setAlert, setVisible, varRefeshPage, setVarRefeshPage) => async dispatch => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var requestOptions = {
        method: 'PUT',
        redirect: 'follow',
        headers: myHeaders,
        body: JSON.stringify(editData)
    }
    try {
        const response = await fetch(`http://localhost:8000/orders/${orderId}`, requestOptions);
        const data = await response.json();
        if (data.status) {
            setDisplayAlert(true)
            settextAlert("Update order successful")
            setAlert("success")
            setVisible(false)
            setVarRefeshPage(varRefeshPage + 1)
        }
        return dispatch({
            type: EDIT_ORDER,
            payload: data
        });
    }
    catch (error) {
        console.log(error)
    }
}

export const deleteOrderAction = (orderId, setDisplayAlert, settextAlert, setAlert, setVisible, varRefeshPage, setVarRefeshPage) => async dispatch => {
    var requestOptions = {
        method: 'GET',
        redirect: 'follow'
    }
    try {
        const response = await fetch(`http://localhost:8000/customers`, requestOptions);
        const data = await response.json();
        console.log( data)
        var result = data.customer.filter(item => {
            if (item.orders.length != []) {
                return item.orders.find((a) => a._id == orderId)
            }
        })
        
        if (result) {
            var myHeadersOrder = new Headers();
            myHeadersOrder.append("Content-Type", "application/json");
            var requestOptionsOrder = {
                method: 'DELETE',
                redirect: 'follow',
                headers: myHeadersOrder,
            }
            const responseOrderDelete = await fetch(`http://localhost:8000/customers/${result[0]._id}/orders/${orderId}`, requestOptionsOrder);
            setDisplayAlert(true)
            settextAlert("Delete order successful")
            setAlert("success")
            setVisible(false)
            setVarRefeshPage(varRefeshPage + 1)
        }
        return dispatch({
            type: DELETE_ORDER,
            payload: data
        });
    }
    catch (error) {
        console.log(error)
    }
}

