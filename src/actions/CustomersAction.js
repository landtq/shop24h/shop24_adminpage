import { ADD_CUSTOMER, DELETE_CUSTOMER, EDIT_CUSTOMER, GET_ALL_CUSTOMER, GET_DISTRICT, GET_PROVINCE, PAGINATION_CUSTOMER } from "src/constants/Cus";

export const customerAction = (cusName, limit, currentPage_customer) => async dispatch => {
    var requestOptions = {
        method: 'GET',
        redirect: 'follow'
    }
    try {
        const response = await fetch(`http://localhost:8000/customers?filterName=${cusName}&&limit=${limit}&&page=${currentPage_customer}`, requestOptions);
        const data = await response.json();
        return dispatch({
            type: GET_ALL_CUSTOMER,
            payload: {
                total: data.total,
                limit: data.limit,
                customer: data.customer,
            }
        });
    }
    catch (error) {
        console.log(error)
    }
}

export const ChangeNoPagecustomer = (value) => {
    return {
        type: PAGINATION_CUSTOMER,
        payload: value
    }
}

export const addCusAction = (addData, setDisplayAlert, settextAlert, setAlert, setVisible, varRefeshPage, setVarRefeshPage) => async dispatch => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var requestOptions = {
        method: 'POST',
        redirect: 'follow',
        headers: myHeaders,
        body: JSON.stringify(addData)
    }
    try {
        const response = await fetch(`http://localhost:8000/customers`, requestOptions);
        const data = await response.json();
        console.log(data)
        if (data.message) {
            setDisplayAlert(true)
            settextAlert("Add new customer successful")
            setAlert("success")
            setVisible(false)
            setVarRefeshPage(varRefeshPage + 1)
        }
        return dispatch({
            type: ADD_CUSTOMER,
            payload: data
        });
    }
    catch (error) {
        console.log(error)
    }
}

export const editCusAction = (editData, cusId, setDisplayAlert, settextAlert, setAlert, setVisible, varRefeshPage, setVarRefeshPage) => async dispatch => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var requestOptions = {
        method: 'PUT',
        redirect: 'follow',
        headers: myHeaders,
        body: JSON.stringify(editData)
    }
    try {
        const response = await fetch(`http://localhost:8000/customers/${cusId}`, requestOptions);
        const data = await response.json();
        if (data.message) {
            setDisplayAlert(true)
            settextAlert("Update customer successful")
            setAlert("success")
            setVisible(false)
            setVarRefeshPage(varRefeshPage + 1)
        }
        return dispatch({
            type: EDIT_CUSTOMER,
            payload: data
        });
    }
    catch (error) {
        console.log(error)
    }
}

export const deleteCusAction = (cusId, setDisplayAlert, settextAlert, setAlert, setVisible, varRefeshPage, setVarRefeshPage) => async dispatch => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var requestOptions = {
        method: 'DELETE',
        redirect: 'follow',
        headers: myHeaders,
    }
    try {
        const response = await fetch(`http://localhost:8000/customers/${cusId}`, requestOptions);
        setDisplayAlert(true)
        settextAlert("Delete customer successful")
        setAlert("success")
        setVisible(false)
        setVarRefeshPage(varRefeshPage + 1)

        return dispatch({
            type: DELETE_CUSTOMER,
        });
    }
    catch (error) {
        console.log(error)
    }
}

export const getProvince = () => async dispatch => {
    var requestOptions = {
        method: 'GET',
        redirect: 'follow'
    }
    try {
        const response = await fetch(`https://provinces.open-api.vn/api/`, requestOptions);
        const data = await response.json();
        return dispatch({
            type: GET_PROVINCE,
            payload: data
        });
    }
    catch (error) {
        console.log(error)
    }

}
export const getDistrict = (code) => async dispatch => {
    var requestOptions = {
        method: 'GET',
        redirect: 'follow'
    }
    try {
        const response = await fetch(`https://provinces.open-api.vn/api/p/${code}?depth=2`, requestOptions);
        const data = await response.json();
        return dispatch({
            type: GET_DISTRICT,
            payload: data.districts
        });
    }
    catch (error) {
        console.log(error)
    }

}