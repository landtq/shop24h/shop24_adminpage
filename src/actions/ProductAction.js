import { DELETE_PRODUCT, EDIT_PRODUCT, GET_ALL_PRODUCT, PAGINATION_PRODUCT } from "src/constants/Product";

export const productAction = (productName, limit, currentPage_product) => async dispatch => {
    var requestOptions = {
        method: 'GET',
        redirect: 'follow'
    }
    try {
        const response = await fetch(`http://localhost:8000/checkFilter?filterName=${productName}&&limit=${limit}&&page=${currentPage_product}`, requestOptions);
        const data = await response.json();
        return dispatch({
            type: GET_ALL_PRODUCT,
            payload: {
                total: data.total,
                limit: data.limit,
                product: data.product,
            }
        });
    }
    catch (error) {
        console.log(error)
    }
}

export const ChangeNoPageProduct = (value) => {
    return {
        type: PAGINATION_PRODUCT,
        payload: value
    }
}

export const addProductAction = (addData, setDisplayAlert, settextAlert, setAlert, setVisible, varRefeshPage, setVarRefeshPage) => async dispatch => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var requestOptions = {
        method: 'POST',
        redirect: 'follow',
        headers: myHeaders,
        body: JSON.stringify(addData)
    }
    try {
        const response = await fetch(`http://localhost:8000/productRouters`, requestOptions);
        const data = await response.json();
        if (data.message) {
            setDisplayAlert(true)
            settextAlert("Add new product successful")
            setAlert("success")
            setVisible(false)
            setVarRefeshPage(varRefeshPage + 1)
        }
        return dispatch({
            type: GET_ALL_PRODUCT,
            payload: data
        });
    }
    catch (error) {
        console.log(error)
    }
}

export const editProductAction = (editData, productId, setDisplayAlert, settextAlert, setAlert, setVisible, varRefeshPage, setVarRefeshPage) => async dispatch => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var requestOptions = {
        method: 'PUT',
        redirect: 'follow',
        headers: myHeaders,
        body: JSON.stringify(editData)
    }
    try {
        const response = await fetch(`http://localhost:8000/productRouters/${productId}`, requestOptions);
        const data = await response.json();
        if (data.message) {
            setDisplayAlert(true)
            settextAlert("Update product successful")
            setAlert("success")
            setVisible(false)
            setVarRefeshPage(varRefeshPage + 1)
        }
        return dispatch({
            type: EDIT_PRODUCT,
            payload: data
        });
    }
    catch (error) {
        console.log(error)
    }
}

export const deleteProductAction = (productId, setDisplayAlert, settextAlert, setAlert, setVisible, varRefeshPage, setVarRefeshPage) => async dispatch => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var requestOptions = {
        method: 'DELETE',
        redirect: 'follow',
        headers: myHeaders,
    }
    try {
        const response = await fetch(`http://localhost:8000/productRouters/${productId}`, requestOptions);
        setDisplayAlert(true)
        settextAlert("Delete product successful")
        setAlert("success")
        setVisible(false)
        setVarRefeshPage(varRefeshPage + 1)

        return dispatch({
            type: DELETE_PRODUCT,
            payload: data
        });
    }
    catch (error) {
        console.log(error)
    }
}