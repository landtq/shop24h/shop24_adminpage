import React from 'react'

const ProductTypes = React.lazy(() => import('./views/ProductTypes/ProductTypes'))
const Products = React.lazy(() => import('./views/Products/Products'))
const Customers = React.lazy(() => import('./views/Customers/Customers'))
const Orders = React.lazy(() => import('./views/Orders/Orders'))

const routes = [
  { path: '/producttypes', name: 'Product Types', element: ProductTypes },
  { path: '/products', name: 'Products', element: Products },
  { path: '/customers', name: 'Customers', element: Customers },
  { path: '/orders', name: 'Orders', element: Orders },
]

export default routes
