import { GET_ALL_CUSTOMER, GET_DISTRICT, GET_PROVINCE, PAGINATION_CUSTOMER } from "src/constants/Cus";

const iniData = {
    currentPage_customer: 1,
    customer: [],
    noPage_customer: 0,
    province: [],
    district: []
}

const customerReducer = (state = iniData, action) => {
    switch (action.type) {
        case PAGINATION_CUSTOMER:
            return {
                ...state,
                currentPage_customer: action.payload
            }
        case GET_ALL_CUSTOMER:
            return {
                ...state,
                customer: action.payload.customer,
                noPage_customer: Math.ceil(action.payload.total / action.payload.limit),
            }
        case GET_PROVINCE:
            return {
                ...state,
                province: action.payload
            }
        case GET_DISTRICT:
            return {
                ...state,
                district: action.payload
            }
        default:
            return state;
    }
}

export default customerReducer;


