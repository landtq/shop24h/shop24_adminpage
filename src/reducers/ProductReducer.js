import { ADD_PRODUCT, EDIT_PRODUCT, GET_ALL_PRODUCT, PAGINATION_PRODUCT } from "src/constants/Product";

const iniData = {
    currentPage_product: 1,
    product: [],
    noPage_product: 0,
    newProduct: {},
    afterEdit: {}
}


const ProductReducer = (state = iniData, action) => {
    switch (action.type) {
        case PAGINATION_PRODUCT:
            return {
                ...state,
                currentPage_product: action.payload
            }
        case GET_ALL_PRODUCT:
            return {
                ...state,
                product: action.payload.product,
                noPage_product: Math.ceil(action.payload.total / action.payload.limit),
            }
        case ADD_PRODUCT:
            return {
                ...state,
                newProduct: action.payload
            }
        case EDIT_PRODUCT:
            return {
                ...state,
                afterEdit: action.payload
            }
        default:
            return state;
    }
}

export default ProductReducer;


