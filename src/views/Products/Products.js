import { CButton, CFormInput, CTable, CTableBody, CTableCaption, CTableDataCell, CTableHead, CTableHeaderCell, CTableRow, cilSearc, CModal, CModalHeader, CModalTitle, CModalBody, CModalFooter, CDropdownItem, CDropdownMenu, CDropdownToggle } from "@coreui/react";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Alert, Grid, Pagination, Snackbar, Stack } from "@mui/material"
import { ChangeNoPageProduct, deleteProductAction, productAction } from "src/actions/ProductAction";
import '../view.css'
import AddProductModal from "./AddProductModal";
import EditProductModel from "./EditProductModal";

const Product = () => {
    var dispatch = useDispatch()
    const [visibleAdd, setVisibleAdd] = useState(false);
    const [visibleEdit, setVisibleEdit] = useState(false);
    const [visibleDelete, setVisibleDelete] = useState(false);
    const { currentPage_product, product, noPage_product } = useSelector((reduxData) => reduxData.ProductReducer);
    const [productName, setProductName] = useState("")
    const [varRefeshPage, setVarRefeshPage] = useState(0);
    const [dataEdit, setDataEdit] = useState("")
    const [idDelete, setIdDelete] = useState("")
    //alert
    const [displayAlert, setDisplayAlert] = useState(false);
    const [textAlert, settextAlert] = useState("text");
    const [typeAlert, setAlert] = useState("error");

    const limit = 5;

    const closeAlert = () => {
        setDisplayAlert(false);
    }

    const changePageHandler = (event, value) => {
        dispatch(ChangeNoPageProduct(value))
    }

    //handleAdd
    const handleAddProduct = () => {
        return setVisibleAdd(!visibleAdd)
    }
    //handleEdit
    const handleEdit = (dataItem) => {
        setDataEdit(dataItem)
        return setVisibleEdit(!visibleEdit);
    }
    //handleDelete
    const handleDelete = (id) => {
        setIdDelete(id)
        return setVisibleDelete(!visibleDelete);
    }
    const handleDeleteClick = () => {
        dispatch(deleteProductAction(idDelete, setDisplayAlert, settextAlert, setAlert, setVisibleDelete, varRefeshPage, setVarRefeshPage))
    }

    useEffect(() => {
        dispatch(productAction(productName, limit,currentPage_product))
    }, [currentPage_product, varRefeshPage,productName])

    return (
        <>
            <AddProductModal
                visible={visibleAdd}
                setVisible={setVisibleAdd}
                varRefeshPage={varRefeshPage}
                setVarRefeshPage={setVarRefeshPage}
            />
            <EditProductModel
                dataEdit={dataEdit}
                setDataEdit={setDataEdit}
                visible={visibleEdit}
                setVisible={setVisibleEdit}
                varRefeshPage={varRefeshPage}
                setVarRefeshPage={setVarRefeshPage}
            />

            {/* delete product */}
            <CModal visible={visibleDelete} onClose={() => setVisibleDelete(false)} >
                <CModalHeader onClose={() => setVisibleDelete(false)}>
                    <CModalTitle style={{ marginLeft: "120px" }}>DELETE THIS PRODUCT ? </CModalTitle>
                </CModalHeader>
                <CModalFooter>
                    <CButton color="primary" onClick={handleDeleteClick} className=" mx-auto">Delete Product</CButton>
                </CModalFooter>
            </CModal>

            <Snackbar
                open={displayAlert}
                onClose={closeAlert}>
                <Stack sx={{ width: '100%' }} spacing={2} >
                    <Alert severity={typeAlert}>{textAlert} </Alert>
                </Stack>
            </Snackbar>

            <h1>All Products</h1>
            <CTable striped hover borderless caption="top" responsive>
                <CTableCaption >
                    <div className="row">
                        <div className="col-sm-10" onClick={handleAddProduct}>
                            <img src="https://img.icons8.com/plasticine/50/000000/plus-2-math.png" style={{ cursor: "pointer" }} /> Add new
                        </div>
                        <div className="col-sm-2">
                            <CFormInput placeholder='search by name' style={{ width: "100%" }} onKeyPress={e=> {setProductName(e.target.value)}} />
                        </div>
                    </div>
                </CTableCaption>
                <CTableHead color="dark">
                    <CTableRow>
                        <CTableHeaderCell scope="col">STT</CTableHeaderCell>
                        <CTableHeaderCell scope="col">Name</CTableHeaderCell>
                        <CTableHeaderCell scope="col">Type</CTableHeaderCell>
                        <CTableHeaderCell scope="col">ImageUrl</CTableHeaderCell>
                        <CTableHeaderCell scope="col">Price</CTableHeaderCell>
                        <CTableHeaderCell scope="col">Discount</CTableHeaderCell>
                        <CTableHeaderCell scope="col">Color</CTableHeaderCell>
                        <CTableHeaderCell scope="col">Categories</CTableHeaderCell>
                        <CTableHeaderCell scope="col">Action</CTableHeaderCell>
                    </CTableRow>
                </CTableHead>
                <CTableBody>
                    {product? product.map((item, index) => {
                        return (
                            <CTableRow key={index} >
                                <CTableHeaderCell scope="row">{index + 1}</CTableHeaderCell>
                                <CTableDataCell>{item.name}</CTableDataCell>
                                <CTableDataCell>{item.type.name}</CTableDataCell>
                                <CTableDataCell>
                                    <img src={item.imageUrl} width="100vw" alt="" />
                                </CTableDataCell>
                                <CTableDataCell>{item.buyPrice}</CTableDataCell>
                                <CTableDataCell>{item.promotionPrice}</CTableDataCell>
                                <CTableDataCell>
                                    {item.color.map((color, indexColor) => {
                                        return (
                                            <div key={indexColor}>
                                                <p>
                                                    {color}
                                                </p>
                                            </div>
                                        )
                                    })}
                                </CTableDataCell>
                                <CTableDataCell>
                                    {item.categories.map((categories, indexcategories) => {
                                        return (
                                            <div key={indexcategories}>
                                                <p>
                                                    {categories}
                                                </p>
                                            </div>
                                        )
                                    })}
                                </CTableDataCell>
                                <CTableDataCell ><img src="https://img.icons8.com/stickers/24/000000/pencil.png" style={{ cursor: "pointer" }} onClick={() => handleEdit(item)} />&emsp;<img src="https://img.icons8.com/external-anggara-filled-outline-anggara-putra/24/000000/external-delete-ecommerce-anggara-filled-outline-anggara-putra.png" style={{ cursor: "pointer" }} onClick={() => handleDelete(item._id)} /></CTableDataCell>
                            </CTableRow>
                        )
                    }): null}
                </CTableBody>
            </CTable>
            <Grid container mt={3} justifyContent="flex-end">
                <Grid item>
                    <Pagination count={noPage_product} defaultPage={1} onChange={changePageHandler} color="primary" variant="outlined" shape="rounded" />
                </Grid>
            </Grid>
        </>
    )
}
export default Product; 