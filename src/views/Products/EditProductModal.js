import { CButton, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle } from "@coreui/react";
import { Alert, Box, Checkbox, FormControl, FormControlLabel, FormGroup, FormHelperText, FormLabel, Input, InputLabel, NativeSelect, Snackbar, Stack, TextField } from "@mui/material";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { editProductAction } from "src/actions/ProductAction";


const EditProductModel = ({ dataEdit, setDataEdit, visible, setVisible, varRefeshPage, setVarRefeshPage }) => {
    var dispatch = useDispatch()
    //data
    const catagoriesP = ["Chairs", "Beds", "Accesories", "Furniture", "Home Deco", "Dressings", "Tables"];
    const typeProduct = [
        { name: 'Amado', id: "63235b4a1cdf1929cd3ea52b" },
        { name: 'Ikea', id: "63235b551cdf1929cd3ea52e" },
        { name: 'Artdeco', id: "63515a15f800f05b200d70d7" },
        { name: 'The factory', id: "63515a07f800f05b200d70d6" },
        { name: 'Furniture Inc', id: "63235b601cdf1929cd3ea531" }
    ];
    const colorProduct = ['red', 'blue', 'gray', 'brow', 'black', 'white', 'orange', 'yellow'];


    const [colorArr, setColorArr] = useState([])
    const [categoriesArr, setcategoriesArr] = useState([])

    const onCheckBoxColorClick = (event) => {
        setColorArr([...colorArr, event.target.value])
    };
    const onCheckBoxCategoriesClick = (event) => {
        setcategoriesArr([...categoriesArr, event.target.value])
    }
    //alert
    const [displayAlert, setDisplayAlert] = useState(false);
    const [textAlert, settextAlert] = useState("text");
    const [typeAlert, setAlert] = useState("error")  
    const closeAlert = () => {
        setDisplayAlert(false);
    }
    //change input
    const handleChange = (event) => {
        setDataEdit({
            ...dataEdit,
            [event.target.name]: event.target.value
        })
    };

    //click edit
    const handleEditClick = () => {
        var validateResult = validateInp()
        if (validateResult) {
            var editData = {
                name: dataEdit.name,
                imageUrl: dataEdit.imageUrl,
                buyPrice: Number(dataEdit.buyPrice),
                promotionPrice: Number(dataEdit.promotionPrice),
                imageChild: dataEdit.imageChild,
                color: [...new Set(colorArr)],
                categories: [...new Set(categoriesArr)],
                type: dataEdit.type._id,
            }
            dispatch(editProductAction(editData, dataEdit._id, setDisplayAlert, settextAlert, setAlert, setVisible, varRefeshPage, setVarRefeshPage));
        }
    }

    const validateInp = () => {
        if (dataEdit.name == "") {
            setDisplayAlert(true);
            settextAlert("please enter product name")
            return false
        }
        if (dataEdit.imageUrl == "") {
            setDisplayAlert(true);
            settextAlert("please enter imageUrl")
            return false
        }

        if (Number(dataEdit.promotionPrice) >= Number(dataEdit.buyPrice)) {
            setDisplayAlert(true);
            settextAlert("promotion price invalid")
            return false
        }
        if (dataEdit.imageChild.length == 0) {
            setDisplayAlert(true);
            settextAlert("please enter imageChild")
            return false
        }
        if (colorArr.length == 0) {
            setDisplayAlert(true);
            settextAlert("please select color")
            return false
        }
        if (categoriesArr.length == 0) {
            setDisplayAlert(true);
            settextAlert("please select categories")
            return false
        }
        return true
    }

    return (
        <>
            <Snackbar
                open={displayAlert}
                onClose={closeAlert}>
                <Stack sx={{ width: '100%' }} spacing={2} >
                    <Alert severity={typeAlert}>{textAlert} </Alert>
                </Stack>
            </Snackbar>

            <CModal visible={visible} onClose={() => setVisible(false)} >
                <CModalHeader onClose={() => setVisible(false)}>
                    <CModalTitle style={{ marginLeft: "120px" }}>EDIT DATA PRODUCT</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    <div style={{ textAlign: "center" }}>
                        <Box component="form" sx={{ '& > :not(style)': { m: 1 } }}  >
                            <FormControl focused variant="standard" sx={{ width: "300px" }}>
                                <InputLabel htmlFor="component-error">Name</InputLabel>
                                <Input onChange={handleChange} aria-describedby="component-error-text" name="name" value={dataEdit.name} />
                            </FormControl>
                        </Box>
                        <Box component="form" sx={{ '& > :not(style)': { m: 1 } }} >
                            <FormControl focused variant="standard" sx={{ width: "300px" }}>
                                <InputLabel htmlFor="component-error">ImageUrl</InputLabel>
                                <Input onChange={handleChange} aria-describedby="component-error-text" name="imageUrl" value={dataEdit.imageUrl} />
                            </FormControl>
                        </Box>
                        <Box sx={{ '& > :not(style)': { m: 1 } }}  >
                            <FormControl focused variant="standard" sx={{ width: "300px" }}>
                                <InputLabel htmlFor="standard-adornment-amount" >BuyPrice</InputLabel>
                                <Input onChange={handleChange} aria-describedby="component-error-text" name="buyPrice" value={new Number(dataEdit.buyPrice)} type="number" />
                            </FormControl>
                        </Box>
                        <Box component="form" sx={{ '& > :not(style)': { m: 1 } }} >
                            <FormControl focused variant="standard" sx={{ width: "300px" }}>
                                <InputLabel htmlFor="component-error">PromotionPrice</InputLabel>
                                <Input onChange={handleChange} aria-describedby="component-error-text" name="promotionPrice" value={Number(dataEdit.promotionPrice)} type="number" />
                            </FormControl>
                        </Box>
                        <Box component="form" sx={{ '& > :not(style)': { m: 1 } }} >
                            <FormControl focused variant="standard" sx={{ width: "300px" }}>
                                <InputLabel htmlFor="component-error">ImageChild</InputLabel>
                                <Input onChange={handleChange} aria-describedby="component-error-text" name="imageChild" value={dataEdit.imageChild} />
                            </FormControl>
                        </Box>
                        <Box>
                            <FormControl sx={{ m: 3 }} component="fieldset" variant="standard">
                                <FormLabel component="legend" focused>Color</FormLabel>
                                <FormGroup sx={{ marginLeft: "50px" }}>
                                    {colorProduct.map((item, index) => {
                                        return (
                                            <FormControlLabel
                                                value={item}
                                                control={<Checkbox />}
                                                label={item}
                                                labelPlacement="end"
                                                key={index}
                                                onChange={onCheckBoxColorClick}
                                            />
                                        )
                                    })}
                                </FormGroup>
                            </FormControl>
                            <FormControl sx={{ m: 3 }} component="fieldset" variant="standard">
                                <FormLabel component="legend" focused >Categories</FormLabel>
                                <FormGroup sx={{ marginLeft: "50px" }}>
                                    {catagoriesP.map((item, index) => {
                                        return (
                                            <FormControlLabel
                                                value={item}
                                                control={<Checkbox />}
                                                label={item}
                                                labelPlacement="end"
                                                key={index}
                                                onChange={onCheckBoxCategoriesClick}
                                            />
                                        )
                                    })}
                                </FormGroup>
                            </FormControl>
                        </Box>
                        <Box >
                            <FormControl >
                                <InputLabel variant="standard" htmlFor="uncontrolled-native">
                                    Type
                                </InputLabel>
                                <NativeSelect sx={{ width: "320px" }} onChange={handleChange} name="type" value={dataEdit.type?._id}
                                >
                                    {typeProduct.map((item, index) => {
                                        return (
                                            <option value={item.id} key={index}>{item.name}</option>
                                        )
                                    })}
                                </NativeSelect>
                            </FormControl>
                        </Box>
                    </div>

                </CModalBody>
                <CModalFooter>
                    <CButton color="secondary" onClick={() => setVisible(false)}>
                        Close
                    </CButton>
                    <CButton color="primary" onClick={handleEditClick}>Update Product</CButton>
                </CModalFooter>
            </CModal>
        </>
    )
}

export default EditProductModel; 