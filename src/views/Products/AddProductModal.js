import { CButton, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle } from "@coreui/react";
import { Alert, Box, FormControl, Input, InputLabel, NativeSelect, Snackbar, Stack } from "@mui/material";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { addProductAction } from "src/actions/ProductAction";

const AddProductModal = ({ visible, setVisible, closeModelAdd, varRefeshPage, setVarRefeshPage }) => {
    var dispatch = useDispatch()

    //data
    const catagoriesP = ["Chairs", "Beds", "Accesories", "Furniture", "Home Deco", "Dressings", "Tables"];
    const typeProduct = [
        { name: 'Amado', id: "63235b4a1cdf1929cd3ea52b" },
        { name: 'Ikea', id: "63235b551cdf1929cd3ea52e" },
        { name: 'Artdeco', id: "63515a15f800f05b200d70d7" },
        { name: 'The factory', id: "63515a07f800f05b200d70d6" },
        { name: 'Furniture Inc', id: "63235b601cdf1929cd3ea531" }
    ];
    const colorProduct = ['red', 'blue', 'gray', 'brow', 'black', 'white', 'orange', 'yellow'];


    //alert
    const [displayAlert, setDisplayAlert] = useState(false);
    const [textAlert, settextAlert] = useState("text");
    const [typeAlert, setAlert] = useState("error")
    const closeAlert = () => {
        setDisplayAlert(false);
    }
    const handleChange = (event) => {
        setDataInp({
            ...dataInp,
            [event.target.name]: event.target.value
        })
    };
    const [dataInp, setDataInp] = useState({
        name: "",
        description: "",
        imageUrl: "",
        imageChild: "",
    })
    const [color, setColor] = useState("red")
    const [categories, setCategories] = useState("Chairs")
    const [type, setType] = useState("63235b4a1cdf1929cd3ea52b")
    const [buyPrice, setbuyPrice] = useState(0)
    const [promotionPrice, setpromotionPrice] = useState(0)

    const handleAddClick = () => {
        var validateResult = validateInp()
        if (validateResult) {
            var addData = { ...dataInp, color, type, categories, buyPrice, promotionPrice }
            dispatch(addProductAction(addData, setDisplayAlert, settextAlert, setAlert, setVisible, varRefeshPage, setVarRefeshPage));
        }
    }
    const validateInp = () => {
        if (dataInp.name == "") {
            setDisplayAlert(true);
            settextAlert("please enter product name")
            return false
        }
        if (dataInp.description == "") {
            setDisplayAlert(true);
            settextAlert("please enter product description")
            return false
        }
        if (dataInp.imageUrl == "") {
            setDisplayAlert(true);
            settextAlert("please enter imageUrl")
            return false
        }
        if (isNaN(buyPrice) || buyPrice <= 0) {
            setDisplayAlert(true);
            settextAlert("price invalid")
            return false
        }
        if (isNaN(promotionPrice) || promotionPrice <= 0 || promotionPrice >= buyPrice) {
            setDisplayAlert(true);
            settextAlert("promotion price invalid")
            return false
        }
        if (dataInp.imageChild == "") {
            setDisplayAlert(true);
            settextAlert("please enter imageChild")
            return false
        }
        return true
    }
    return (
        <>
            <Snackbar
                open={displayAlert}
                onClose={closeAlert}>
                <Stack sx={{ width: '100%' }} spacing={2} >
                    <Alert severity={typeAlert}>{textAlert} </Alert>
                </Stack>
            </Snackbar>

            <CModal visible={visible} onClose={() => setVisible(false)} >
                <CModalHeader onClose={() => setVisible(false)}>
                    <CModalTitle style={{ marginLeft: "120px" }}>ADD NEW PRODUCT</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    <div style={{ textAlign: "center" }}>
                        <Box component="form" sx={{ '& > :not(style)': { m: 1 } }}  >
                            <FormControl focused variant="standard" sx={{ width: "300px" }}>
                                <InputLabel htmlFor="component-error">Name</InputLabel>
                                <Input onChange={handleChange} aria-describedby="component-error-text" name="name" />
                            </FormControl>
                        </Box>
                        <Box component="form" sx={{ '& > :not(style)': { m: 1 } }} >
                            <FormControl focused variant="standard" sx={{ width: "300px" }}>
                                <InputLabel htmlFor="component-error">Description</InputLabel>
                                <Input onChange={handleChange} aria-describedby="component-error-text" name="description" />
                            </FormControl>
                        </Box>
                        <Box component="form" sx={{ '& > :not(style)': { m: 1 } }} >
                            <FormControl focused variant="standard" sx={{ width: "300px" }}>
                                <InputLabel htmlFor="component-error">ImageUrl</InputLabel>
                                <Input onChange={handleChange} aria-describedby="component-error-text" name="imageUrl" />
                            </FormControl>
                        </Box>
                        <Box sx={{ '& > :not(style)': { m: 1 } }}  >
                            <FormControl focused variant="standard" sx={{ width: "300px" }}>
                                <InputLabel htmlFor="standard-adornment-amount" >BuyPrice</InputLabel>
                                <Input onChange={(e) => { setbuyPrice(Number(e.target.value)) }} name="buyPrice" />
                            </FormControl>
                        </Box>
                        <Box component="form" sx={{ '& > :not(style)': { m: 1 } }} >
                            <FormControl focused variant="standard" sx={{ width: "300px" }}>
                                <InputLabel htmlFor="component-error">PromotionPrice</InputLabel>
                                <Input name="promotionPrice" onChange={(e) => { setpromotionPrice(Number(e.target.value)) }} />
                            </FormControl>
                        </Box>
                        <Box component="form" sx={{ '& > :not(style)': { m: 1 } }} >
                            <FormControl focused variant="standard" sx={{ width: "300px" }}>
                                <InputLabel htmlFor="component-error">ImageChild</InputLabel>
                                <Input onChange={handleChange} aria-describedby="component-error-text" name="imageChild" />
                            </FormControl>
                        </Box>

                        <div className="row mt-3" style={{ marginLeft: "60px" }}>
                            <div className="col-sm-5">
                                <Box >
                                    <FormControl >
                                        <InputLabel variant="standard" htmlFor="uncontrolled-native">
                                            Type
                                        </InputLabel>
                                        <NativeSelect sx={{ width: "120px" }} onChange={(event) => { setType(event.target.value) }}
                                        >
                                            {typeProduct.map((item, index) => {
                                                return (
                                                    <option value={item.id} key={index}>{item.name}</option>
                                                )
                                            })}
                                        </NativeSelect>
                                    </FormControl>
                                </Box>
                            </div>
                            <div className="col-sm-5">
                                <Box >
                                    <FormControl >
                                        <InputLabel variant="standard" htmlFor="uncontrolled-native">
                                            Color
                                        </InputLabel>
                                        <NativeSelect sx={{ width: "120px" }} defaultValue="red" onChange={(event) => { setColor(event.target.value) }}
                                        >
                                            {colorProduct.map((item, index) => {
                                                return (
                                                    <option value={item} key={index}>{item}</option>
                                                )
                                            })}
                                        </NativeSelect>
                                    </FormControl>
                                </Box>
                            </div>
                        </div>
                        <Box mt={3}>
                            <FormControl >
                                <InputLabel variant="standard" htmlFor="uncontrolled-native">
                                    Categories
                                </InputLabel>
                                <NativeSelect sx={{ width: "290px" }} defaultValue="Chairs" onChange={(event) => { setCategories(event.target.value) }}
                                >
                                    {catagoriesP.map((item, index) => {
                                        return (
                                            <option value={item} key={index}>{item}</option>
                                        )
                                    })}
                                </NativeSelect>
                            </FormControl>
                        </Box>
                    </div>

                </CModalBody>
                <CModalFooter>
                    <CButton color="secondary" onClick={() => setVisible(false)}>
                        Close
                    </CButton>
                    <CButton color="primary" onClick={handleAddClick}>Save Product</CButton>
                </CModalFooter>
            </CModal>
        </>
    )
}

export default AddProductModal;