import { CButton, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle } from "@coreui/react";
import { Alert, Box, FormControl, Input, InputLabel, Snackbar, Stack } from "@mui/material";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { editOrderAction } from "src/actions/OrderAction";

const EditOrderModel = ({ dataEdit, setDataEdit, visible, setVisible, varRefeshPage, setVarRefeshPage }) => {
    var dispatch = useDispatch()
    //alert
    const [displayAlert, setDisplayAlert] = useState(false);
    const [textAlert, settextAlert] = useState("text");
    const [typeAlert, setAlert] = useState("error")
    const closeAlert = () => {
        setDisplayAlert(false);
    }

    //change input
    const handleChange = (event) => {
        setDataEdit({
            ...dataEdit,
            [event.target.name]: event.target.value
        })
    };

    //click edit
    const handleEditClick = () => {
        var validateResult = validateInp()
        if (validateResult) {
            var editData = {
                orderDate: dataEdit.orderDate,
                shippedDate: dataEdit.shippedDate,
                cost: Number(dataEdit.cost),
                note: dataEdit.note
            }
            dispatch(editOrderAction(editData, dataEdit._id, setDisplayAlert, settextAlert, setAlert, setVisible, varRefeshPage, setVarRefeshPage));
        }
    }

    const validateInp = () => {
        if (dataEdit.orderDate == "") {
            setDisplayAlert(true);
            settextAlert("please enter order date")
            return false
        }
        if (dataEdit.orderDate == "") {
            setDisplayAlert(true);
            settextAlert("please enter delivery date")
            return false
        }
        
        if (isNaN(dataEdit.cost) || dataEdit.cost <= 0) {
            setDisplayAlert(true);
            settextAlert("cost invalid")
            return false
        }
        return true
    }

    return (
        <>
            <Snackbar
                open={displayAlert}
                onClose={closeAlert}>
                <Stack sx={{ width: '100%' }} spacing={2} >
                    <Alert severity={typeAlert}>{textAlert} </Alert>
                </Stack>
            </Snackbar>

            <CModal visible={visible} onClose={() => setVisible(false)} >
                <CModalHeader onClose={() => setVisible(false)}>
                    <CModalTitle style={{ marginLeft: "120px" }}>EDIT DATA ORDER</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    <div style={{ textAlign: "center" }}>
                        <Box component="form" sx={{ '& > :not(style)': { m: 1 } }}  >
                            <FormControl focused variant="standard" sx={{ width: "300px" }}>
                                <InputLabel htmlFor="component-error">Order Date</InputLabel>
                                <Input onChange={handleChange} aria-describedby="component-error-text" name="orderDate" value={dataEdit.orderDate} />
                            </FormControl>
                        </Box>
                        <Box component="form" sx={{ '& > :not(style)': { m: 1 } }} >
                            <FormControl focused variant="standard" sx={{ width: "300px" }}>
                                <InputLabel htmlFor="component-error">Delivery Date</InputLabel>
                                <Input onChange={handleChange} aria-describedby="component-error-text" name="shippedDate" value={dataEdit.shippedDate} />
                            </FormControl>
                        </Box>
                        <Box component="form" sx={{ '& > :not(style)': { m: 1 } }} >
                            <FormControl focused variant="standard" sx={{ width: "300px" }}>
                                <InputLabel htmlFor="component-error">Note</InputLabel>
                                <Input onChange={handleChange} aria-describedby="component-error-text" name="note" value={dataEdit.note} />
                            </FormControl>
                        </Box>
                        <Box sx={{ '& > :not(style)': { m: 1 } }}  >
                            <FormControl focused variant="standard" sx={{ width: "300px" }}>
                                <InputLabel htmlFor="standard-adornment-amount" >Cost</InputLabel>
                                <Input onChange={handleChange} aria-describedby="component-error-text" name="cost" value={dataEdit.cost} />
                            </FormControl>
                        </Box>
                    </div>

                </CModalBody>
                <CModalFooter>
                    <CButton color="secondary" onClick={() => setVisible(false)}>
                        Close
                    </CButton>
                    <CButton color="primary" onClick={handleEditClick}>Update Order</CButton>
                </CModalFooter>
            </CModal>
        </>
    )
}

export default EditOrderModel; 