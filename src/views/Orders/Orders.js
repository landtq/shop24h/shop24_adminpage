import { CButton, CFormInput, CModal, CModalFooter, CModalHeader, CModalTitle, CTable, CTableBody, CTableCaption, CTableDataCell, CTableHead, CTableHeaderCell, CTableRow } from "@coreui/react";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Alert, Grid, Pagination, Snackbar, Stack } from "@mui/material"
import { ChangeNoPageOrder, deleteOrderAction, orderAction } from "src/actions/OrderAction";
import AddOrderModel from "./AddOrderModel";
import EditOrderModel from "./EditOrderModel";
import moment from 'moment'
import OrderDetailModel from "./OrderDetailModel";
import 'bootstrap/dist/css/bootstrap.min.css'

const Order = () => {
    const dispatch = useDispatch()
    const { currentPage_order, order, noPage_order } = useSelector((reduxData) => reduxData.orderReducer);
    const [varRefeshPage, setVarRefeshPage] = useState(0);
    const [filterId, setFilterId] = useState("")
    const [dataEdit, setDataEdit] = useState("")
    const [idDelete, setIdDelete] = useState("")
    const [visibleDelete, setVisibleDelete] = useState(false);
    const [visibleEdit, setVisibleEdit] = useState(false);
    const [visibleAdd, setVisibleAdd] = useState(false);
    const [visibleOrderDetail, setVisibleOrderDetail] = useState(false);
    const [dataOrderDetail, setOrderDetail] = useState(null)

    const changePageHandler = (event, value) => {
        dispatch(ChangeNoPageOrder(value))
    }

    const [displayAlert, setDisplayAlert] = useState(false);
    const [textAlert, settextAlert] = useState("text");
    const [typeAlert, setAlert] = useState("error")
    const closeAlert = () => {
        setDisplayAlert(false);
    }
    //handleAdd
    const handleAddOrder = () => {
        return setVisibleAdd(!visibleAdd)
    }
    //handleEdit
    const handleEdit = (dataItem) => {
        setDataEdit(dataItem)
        return setVisibleEdit(!visibleEdit);
    }
    const handleDelete = (id) => {
        setIdDelete(id)
        return setVisibleDelete(!visibleDelete);
    }
    const handleDeleteClick = () => {
        dispatch(deleteOrderAction(idDelete, setDisplayAlert, settextAlert, setAlert, setVisibleDelete, varRefeshPage, setVarRefeshPage))
    }
    //handleOrderDetail
    const handleOrderDetail = (item) => {
        setOrderDetail(item)
        return setVisibleOrderDetail(true)
    }

    useEffect(() => {
        dispatch(orderAction(filterId))
    }, [currentPage_order, varRefeshPage, filterId])

    return (
        <>
            <AddOrderModel
                visible={visibleAdd}
                setVisible={setVisibleAdd}
                varRefeshPage={varRefeshPage}
                setVarRefeshPage={setVarRefeshPage}
            />
            <EditOrderModel
                dataEdit={dataEdit}
                setDataEdit={setDataEdit}
                visible={visibleEdit}
                setVisible={setVisibleEdit}
                varRefeshPage={varRefeshPage}
                setVarRefeshPage={setVarRefeshPage}
            />
            <OrderDetailModel
                dataOrderDetail={dataOrderDetail}
                visibleOrderDetail={visibleOrderDetail}
                setVisibleOrderDetail={setVisibleOrderDetail}
                varRefeshPage={varRefeshPage}
                setVarRefeshPage={setVarRefeshPage}
            />
            <Snackbar
                open={displayAlert}
                onClose={closeAlert}>
                <Stack sx={{ width: '100%' }} spacing={2} >
                    <Alert severity={typeAlert}>{textAlert} </Alert>
                </Stack>
            </Snackbar>
            <CModal visible={visibleDelete} onClose={() => setVisibleDelete(false)} >
                <CModalHeader onClose={() => setVisibleDelete(false)}>
                    <CModalTitle style={{ marginLeft: "120px" }}>DELETE THIS ORDER ? </CModalTitle>
                </CModalHeader>
                <CModalFooter>
                    <CButton color="primary" onClick={handleDeleteClick} className=" mx-auto">Delete Product</CButton>
                </CModalFooter>
            </CModal>

            <h1>All Orders</h1>
            <CTable striped hover borderless caption="top" responsive >
                <CTableCaption >
                    <div className="row">
                        <div className="col-sm-10">
                            <img src="https://img.icons8.com/plasticine/50/000000/plus-2-math.png" style={{ cursor: "pointer" }} onClick={handleAddOrder} /> Add new
                        </div>
                        <div className="col-sm-2">
                            <CFormInput placeholder='search by ID' style={{ width: "100%" }} onChange={(e) => setFilterId(e.target.value)} />
                        </div>
                    </div>
                </CTableCaption>
                <CTableHead color="dark">
                    <CTableRow>
                        <CTableHeaderCell scope="col">STT</CTableHeaderCell>
                        <CTableHeaderCell scope="col">Order Code</CTableHeaderCell>
                        <CTableHeaderCell scope="col">Date</CTableHeaderCell>
                        <CTableHeaderCell scope="col">Shipped Date</CTableHeaderCell>
                        <CTableHeaderCell scope="col">Note</CTableHeaderCell>
                        <CTableHeaderCell scope="col">OrderDetails</CTableHeaderCell>
                        <CTableHeaderCell scope="col">Cost</CTableHeaderCell>
                        <CTableHeaderCell scope="col">Action</CTableHeaderCell>
                    </CTableRow>
                </CTableHead>
                <CTableBody>
                    {order.length > 0 ? order.map((item, index) => {
                        return (
                            <CTableRow key={index}>
                                <CTableHeaderCell scope="row">{index + 1}</CTableHeaderCell>
                                <CTableDataCell>{item._id}</CTableDataCell>
                                <CTableDataCell>{moment(item.orderDate).format('MM-DD-YYYY')}</CTableDataCell>
                                <CTableDataCell>{moment(item.shippedDate).format('MM-DD-YYYY')}</CTableDataCell>
                                <CTableDataCell >{item.note}</CTableDataCell>
                                <CTableDataCell  >
                                    <p onClick={() => handleOrderDetail(item.orderDetails)} style={{ color: "green", textDecoration: "underline", cursor: "pointer", marginLeft: "30px" }} >View</p>
                                </CTableDataCell>
                                <CTableDataCell>{item.cost}</CTableDataCell>
                                <CTableDataCell ><img src="https://img.icons8.com/stickers/24/000000/pencil.png" style={{ cursor: "pointer" }} onClick={() => handleEdit(item)} />&emsp;<img src="https://img.icons8.com/external-anggara-filled-outline-anggara-putra/24/000000/external-delete-ecommerce-anggara-filled-outline-anggara-putra.png" style={{ cursor: "pointer" }} onClick={() => handleDelete(item._id)} /></CTableDataCell>
                            </CTableRow>
                        )
                    }) : null}
                </CTableBody>
            </CTable>
            <Grid container mt={3} justifyContent="flex-end">
                <Grid item>
                    <Pagination count={noPage_order} defaultPage={1} onChange={changePageHandler} color="primary" variant="outlined" shape="rounded" />
                </Grid>
            </Grid>

        </>
    )
}
export default Order; 