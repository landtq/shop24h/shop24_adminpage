import * as React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import { CDBCard, CDBCardBody, CDBDataTable, CDBRow, CDBCol, CDBContainer } from 'cdbreact';
import moment from 'moment'

const style = {
    position: 'absolute',
    top: '30%',
    left: '55%',
    transform: 'translate(-50%, -50%)',
    width: 1200,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
    borderRadius: "10px"
};
const OrderDetailModel = ({ dataOrderDetail, visibleOrderDetail, setVisibleOrderDetail, varRefeshPage, setVarRefeshPage }) => {
    var num = 1;
    var dataRoww = []
    if (dataOrderDetail) {
        var productMap = dataOrderDetail.map((item) => item.product)
        if (productMap) {
            productMap.map(itemP => {
                var m = {
                    stt: num++,
                    name: itemP.name,
                    type: itemP.type.name,
                    // quantity : itemP.quantity,
                    price: itemP.buyPrice,
                    date: moment(itemP.createdAt).format('MMMM Do YYYY, h:mm:ss a'),
                    staus: "processing"
                }
                dataRoww.push(m)
            })
        }
    }

    const handleCloseModal = () => {
        setVisibleOrderDetail(false)
    }
    const data = () => {
        return {
            columns: [
                {
                    label: 'No.',
                    field: 'stt',
                    width: 100,
                    attributes: {
                        'aria-controls': 'DataTable',
                        'aria-label': 'no.',
                    },
                },
                {
                    label: 'Name',
                    field: 'name',
                    width: 250,
                },
                {
                    label: 'Type',
                    field: 'type',
                    width: 170,
                },
                // {
                //     label: 'Quantity',
                //     field: 'quantity',
                //     sort: 'asc',
                //     width: 100,
                // },
                {
                    label: 'Price',
                    field: 'price',
                    sort: 'asc',
                    width: 100,
                },
                {
                    label: 'Order date',
                    field: 'date',
                    sort: 'disabled',
                    width: 230,
                },
                {
                    label: 'Status',
                    field: 'staus',
                    sort: 'asc',
                    width: 100,
                },
            ],
            rows: dataRoww,
        };
    };

    return (
        <div>
            <Modal
                open={visibleOrderDetail}
                onClose={handleCloseModal}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography variant="h4" component="h2" color="green" sx={{ textAlign: "center" }}>
                        ORDER DETAIL
                    </Typography>
                    <Typography sx={{ mt: 2 }}>
                        <CDBContainer>
                            <CDBCard>
                                <CDBCardBody>
                                    <CDBDataTable
                                        striped
                                        bordered
                                        hover
                                        scrollY
                                        maxHeight="50vh"
                                        data={data()}
                                        materialSearch
                                    />
                                </CDBCardBody>
                            </CDBCard>
                        </CDBContainer>
                    </Typography>
                </Box>
            </Modal>
        </div>
    )
}

export default OrderDetailModel;