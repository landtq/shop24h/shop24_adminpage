import { CButton, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle } from "@coreui/react";
import { Alert, Box, FormControl, Input, InputLabel, Snackbar, Stack } from "@mui/material";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { addOrderAction } from "src/actions/OrderAction";

const AddOrderModel = ({ visible, setVisible, varRefeshPage, setVarRefeshPage }) => {
    var dispatch = useDispatch()

    const [cost, setCost] = useState(0)
    const [note, setNote] = useState("")
    const [cusId, seCusId] = useState("")

    //alert
    const [displayAlert, setDisplayAlert] = useState(false);
    const [textAlert, settextAlert] = useState("text");
    const [typeAlert, setAlert] = useState("error")
    const closeAlert = () => {
        setDisplayAlert(false);
    }

    const handleAddClick = () => {
        var validateResult = validateInp()
        if (validateResult) {
            var today = new Date();
            var todasdy = new Date();
            var deliveryDay = today.getDate() + 7;
            todasdy.setDate(deliveryDay)
            var addData = {
                orderDate: today.toUTCString(),
                shippedDate: todasdy.toUTCString(),
                note: note,
                cost: cost
            }
            dispatch(addOrderAction(addData, cusId, setDisplayAlert, settextAlert, setAlert, setVisible, varRefeshPage, setVarRefeshPage));
        }
    }
    const validateInp = () => {
        if (cusId == "") {
            setDisplayAlert(true);
            settextAlert("you must enter cusId")
            return false
        }
        if (isNaN(cost) || cost <= 0) {
            setDisplayAlert(true);
            settextAlert("cost invalid")
            return false
        }
        return true
    }

    return (
        <>
            <Snackbar
                open={displayAlert}
                onClose={closeAlert}>
                <Stack sx={{ width: '100%' }} spacing={2} >
                    <Alert severity={typeAlert}>{textAlert} </Alert>
                </Stack>
            </Snackbar>

            <CModal visible={visible} onClose={() => setVisible(false)} >
                <CModalHeader onClose={() => setVisible(false)}>
                    <CModalTitle style={{ marginLeft: "120px" }}>ADD NEW ORDER</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    <div style={{ textAlign: "center" }}>
                        <Box component="form" sx={{ '& > :not(style)': { m: 1 } }}  >
                            <FormControl focused variant="standard" sx={{ width: "300px" }}>
                                <InputLabel htmlFor="component-error">Add into customer: </InputLabel>
                                <Input aria-describedby="component-error-text" onChange={(e) => { seCusId(e.target.value) }} />
                            </FormControl>
                        </Box>
                        <Box component="form" sx={{ '& > :not(style)': { m: 1 } }}  >
                            <FormControl focused variant="standard" sx={{ width: "300px" }}>
                                <InputLabel htmlFor="component-error">Note</InputLabel>
                                <Input onChange={(e) => { setNote(e.target.value) }} aria-describedby="component-error-text" />
                            </FormControl>
                        </Box>
                        <Box sx={{ '& > :not(style)': { m: 1 } }}  >
                            <FormControl focused variant="standard" sx={{ width: "300px" }}>
                                <InputLabel htmlFor="standard-adornment-amount" >Cost</InputLabel>
                                <Input onChange={(e) => { setCost(Number(e.target.value)) }} />
                            </FormControl>
                        </Box>
                    </div>
                </CModalBody>
                <CModalFooter>
                    <CButton color="secondary" onClick={() => setVisible(false)}>
                        Close
                    </CButton>
                    <CButton color="primary" onClick={handleAddClick}>Save Order</CButton>
                </CModalFooter>
            </CModal>
        </>
    )
}

export default AddOrderModel;