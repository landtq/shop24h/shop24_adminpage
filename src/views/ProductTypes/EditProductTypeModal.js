import { CButton, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle } from "@coreui/react";
import { Alert, Box, FormControl,  Input, InputLabel, Snackbar, Stack } from "@mui/material";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { editProductTypeAction } from "src/actions/ProductTypeAction";

const EditProductTypeModel = ({ dataEdit, setDataEdit, visible, setVisible, varRefeshPage, setVarRefeshPage }) => {
    var dispatch = useDispatch()
    //alert
    const [displayAlert, setDisplayAlert] = useState(false);
    const [textAlert, settextAlert] = useState("text");
    const [typeAlert, setAlert] = useState("error")
    const closeAlert = () => {
        setDisplayAlert(false);
    }
    //change input
    const handleChange = (event) => {
        setDataEdit({
            ...dataEdit,
            [event.target.name]: event.target.value
        })
    };
    //click edit
    const handleEditClick = () => {
        var validateResult = validateInp()
        if (validateResult) {
            var editData = {
                name: dataEdit.name,
                description: dataEdit.description,
            }
            dispatch(editProductTypeAction(editData, dataEdit._id, setDisplayAlert, settextAlert, setAlert, setVisible, varRefeshPage, setVarRefeshPage));
        }
    }
    const validateInp = () => {
        if (dataEdit.name == "") {
            setDisplayAlert(true);
            settextAlert("please enter product name")
            return false
        }
        if (dataEdit.description == "") {
            setDisplayAlert(true);
            settextAlert("please enter product description")
            return false
        }
        return true
    }
    return (
        <>
            <Snackbar
                open={displayAlert}
                onClose={closeAlert}>
                <Stack sx={{ width: '100%' }} spacing={2} >
                    <Alert severity={typeAlert}>{textAlert} </Alert>
                </Stack>
            </Snackbar>
            <CModal visible={visible} onClose={() => setVisible(false)} >
                <CModalHeader onClose={() => setVisible(false)}>
                    <CModalTitle style={{ marginLeft: "120px" }}>EDIT DATA PRODUCT</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    <div style={{ textAlign: "center" }}>
                        <Box component="form" sx={{ '& > :not(style)': { m: 1 } }}  >
                            <FormControl focused variant="standard" sx={{ width: "300px" }}>
                                <InputLabel htmlFor="component-error">Name</InputLabel>
                                <Input onChange={handleChange} aria-describedby="component-error-text" name="name" value={dataEdit.name} />
                            </FormControl>
                        </Box>
                        <Box component="form" sx={{ '& > :not(style)': { m: 1 } }}  >
                            <FormControl focused variant="standard" sx={{ width: "300px" }}>
                                <InputLabel htmlFor="component-error">Description</InputLabel>
                                <Input onChange={handleChange} aria-describedby="component-error-text" name="description" value={dataEdit.description} />
                            </FormControl>
                        </Box>
                    </div>

                </CModalBody>
                <CModalFooter>
                    <CButton color="secondary" onClick={() => setVisible(false)}>
                        Close
                    </CButton>
                    <CButton color="primary" onClick={handleEditClick}>Update Product</CButton>
                </CModalFooter>
            </CModal>
        </>
    )
}
export default EditProductTypeModel; 