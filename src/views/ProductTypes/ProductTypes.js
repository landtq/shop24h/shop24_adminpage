import { CButton, CFormInput, CModal, CModalFooter, CModalHeader, CModalTitle, CRow, CTable, CTableBody, CTableCaption, CTableDataCell, CTableHead, CTableHeaderCell, CTableRow } from "@coreui/react";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { ChangeNoPage, deleteProductTypeAction, productTypeACtion } from "src/actions/ProductTypeAction";

import { Alert, Grid, Pagination, Snackbar, Stack } from "@mui/material"
import AddProductTypeModal from "./AddProductTypeModal";
import EditProductTypeModel from "./EditProductTypeModal";

const ProductType = () => {
    const dispatch = useDispatch()
    const [varRefeshPage, setVarRefeshPage] = useState(0);
    const [dataEdit, setDataEdit] = useState("")
    const [idDelete, setIdDelete] = useState("")
    const { currentPage, productType, noPage } = useSelector((reduxData) => reduxData.ProductTypeReducer);
    const limit = 10;
    const [filterName, setFilterName] = useState("")
    const [visibleDelete, setVisibleDelete] = useState(false);
    const [visibleEdit, setVisibleEdit] = useState(false);
    const [visibleAdd, setVisibleAdd] = useState(false);

    //alert
    const [displayAlert, setDisplayAlert] = useState(false);
    const [textAlert, settextAlert] = useState("text");
    const [typeAlert, setAlert] = useState("error")
    const closeAlert = () => {
        setDisplayAlert(false);
    }
    //handleAdd
    const handleAddProduct = () => {
        return setVisibleAdd(!visibleAdd)
    }
    //handleEdit
    const handleEdit = (dataItem) => {
        setDataEdit(dataItem)
        return setVisibleEdit(!visibleEdit);

    }
    const handleDelete = (id) => {
        setIdDelete(id)
        return setVisibleDelete(!visibleDelete);
    }
    const handleDeleteClick = () => {
        dispatch(deleteProductTypeAction(idDelete, setDisplayAlert, settextAlert, setAlert, setVisibleDelete, varRefeshPage, setVarRefeshPage))
    }

    const changePageHandler = (event, value) => {
        dispatch(ChangeNoPage(value))
    }

    useEffect(() => {
        dispatch(productTypeACtion(filterName,limit,currentPage))
    }, [currentPage, varRefeshPage, filterName])

    return (
        <>
          <AddProductTypeModal
                visible={visibleAdd}
                setVisible={setVisibleAdd}
                varRefeshPage={varRefeshPage}
                setVarRefeshPage={setVarRefeshPage}
            />
            <EditProductTypeModel
                dataEdit={dataEdit}
                setDataEdit={setDataEdit}
                visible={visibleEdit}
                setVisible={setVisibleEdit}
                varRefeshPage={varRefeshPage}
                setVarRefeshPage={setVarRefeshPage}
            />
            <Snackbar
                open={displayAlert}
                onClose={closeAlert}>
                <Stack sx={{ width: '100%' }} spacing={2} >
                    <Alert severity={typeAlert}>{textAlert} </Alert>
                </Stack>
            </Snackbar>
            <CModal visible={visibleDelete} onClose={() => setVisibleDelete(false)} >
                <CModalHeader onClose={() => setVisibleDelete(false)}>
                    <CModalTitle style={{marginLeft: "80px"}}>DELETE THIS PRODUCT TYPE ? </CModalTitle>
                </CModalHeader>
                <CModalFooter>
                    <CButton color="primary" onClick={handleDeleteClick} className=" mx-auto">Delete </CButton>
                </CModalFooter>
            </CModal>

            <h1>Products Types</h1>
            <CTable striped hover border caption="top" responsive className="mt-5">
            <CTableCaption >
                    <div className="row">
                        <div className="col-sm-10">
                            <img src="https://img.icons8.com/plasticine/50/000000/plus-2-math.png" style={{ cursor: "pointer" }} onClick={handleAddProduct} /> Add new
                        </div>
                        <div className="col-sm-2">
                            <CFormInput placeholder='search by name' style={{ width: "100%" }} onChange={e => { setFilterName(e.target.value) }} />
                        </div>
                    </div>
                </CTableCaption>
                <CTableHead color="dark">
                    <CTableRow>
                        <CTableHeaderCell scope="col">STT</CTableHeaderCell>
                        <CTableHeaderCell scope="col">Name</CTableHeaderCell>
                        <CTableHeaderCell scope="col">Description</CTableHeaderCell>
                        <CTableHeaderCell scope="col">Action</CTableHeaderCell>
                    </CTableRow>
                </CTableHead>
                <CTableBody>
                    {productType.map((item, index) => {
                        return (
                            <CTableRow key={index}>
                                <CTableHeaderCell scope="row">{index + 1}</CTableHeaderCell>
                                <CTableDataCell>{item.name}</CTableDataCell>
                                <CTableDataCell>{item.description}</CTableDataCell>
                                <CTableDataCell ><img src="https://img.icons8.com/stickers/24/000000/pencil.png" style={{ cursor: "pointer" }} onClick={() => handleEdit(item)} />&emsp;<img src="https://img.icons8.com/external-anggara-filled-outline-anggara-putra/24/000000/external-delete-ecommerce-anggara-filled-outline-anggara-putra.png" style={{ cursor: "pointer" }} onClick={() => handleDelete(item._id)} /></CTableDataCell>
                            </CTableRow>
                        )
                    })}
                </CTableBody>
            </CTable>
            <Grid container mt={3} justifyContent="flex-end">
                <Grid item>
                    <Pagination count={noPage} defaultPage={1} onChange={changePageHandler} color="primary" variant="outlined" shape="rounded" />
                </Grid>
            </Grid>
        </>
    )
}
export default ProductType; 