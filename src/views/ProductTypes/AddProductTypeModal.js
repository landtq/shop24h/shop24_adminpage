import { CButton, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle } from "@coreui/react";
import { Alert, Box, FormControl, Input, InputLabel, NativeSelect, Snackbar, Stack } from "@mui/material";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { addProductAction } from "src/actions/ProductAction";
import { addProductTypeAction } from "src/actions/ProductTypeAction";

const AddProductTypeModal = ({ visible, setVisible, varRefeshPage, setVarRefeshPage }) => {
    var dispatch = useDispatch()

    //alert
    const [displayAlert, setDisplayAlert] = useState(false);
    const [textAlert, settextAlert] = useState("text");
    const [typeAlert, setAlert] = useState("error")
    const closeAlert = () => {
        setDisplayAlert(false);
    }
    const handleChange = (event) => {
        setDataInp({
            ...dataInp,
            [event.target.name]: event.target.value
        })
    };
    const [dataInp, setDataInp] = useState({
        name: "",
        description: "",
    })

    const handleAddClick = () => {
        var validateResult = validateInp()
        if (validateResult) {
            var addData = { ...dataInp }
            dispatch(addProductTypeAction(addData, setDisplayAlert, settextAlert, setAlert, setVisible, varRefeshPage, setVarRefeshPage));
        }
    }
    const validateInp = () => {
        if (dataInp.name == "") {
            setDisplayAlert(true);
            settextAlert("please enter product name")
            return false
        }
        if (dataInp.description == "") {
            setDisplayAlert(true);
            settextAlert("please enter product description")
            return false
        }
        return true
    }
    return (
        <>
            <Snackbar
                open={displayAlert}
                onClose={closeAlert}>
                <Stack sx={{ width: '100%' }} spacing={2} >
                    <Alert severity={typeAlert}>{textAlert} </Alert>
                </Stack>
            </Snackbar>

            <CModal visible={visible} onClose={() => setVisible(false)} >
                <CModalHeader onClose={() => setVisible(false)}>
                    <CModalTitle style={{ marginLeft: "120px" }}>ADD NEW PRODUCT TYPE</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    <div style={{ textAlign: "center" }}>
                        <Box component="form" sx={{ '& > :not(style)': { m: 1 } }}  >
                            <FormControl focused variant="standard" sx={{ width: "300px" }}>
                                <InputLabel htmlFor="component-error">Name</InputLabel>
                                <Input onChange={handleChange} aria-describedby="component-error-text" name="name" />
                            </FormControl>
                        </Box>
                        <Box component="form" sx={{ '& > :not(style)': { m: 1 } }} >
                            <FormControl focused variant="standard" sx={{ width: "300px" }}>
                                <InputLabel htmlFor="component-error">Description</InputLabel>
                                <Input onChange={handleChange} aria-describedby="component-error-text" name="description" />
                            </FormControl>
                        </Box>
                    </div>
                </CModalBody>
                <CModalFooter>
                    <CButton color="secondary" onClick={() => setVisible(false)}>
                        Close
                    </CButton>
                    <CButton color="primary" onClick={handleAddClick}>Save </CButton>
                </CModalFooter>
            </CModal>
        </>
    )
}

export default AddProductTypeModal;