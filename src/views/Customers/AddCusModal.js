import { CButton, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle } from "@coreui/react";
import { Alert, Box, FormControl, Input, InputLabel, NativeSelect, Snackbar, Stack } from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addCusAction, getDistrict, getProvince } from "src/actions/CustomersAction";

const AddCusModal = ({ visible, setVisible, varRefeshPage, setVarRefeshPage }) => {
    var dispatch = useDispatch()

    const { province, district } = useSelector((data) => data.customerReducer)
    const [sltProvince, setSltProvince] = useState(null)
    const [city, setCity] = useState(null)
    //alert
    const [displayAlert, setDisplayAlert] = useState(false);
    const [textAlert, settextAlert] = useState("text");
    const [typeAlert, setAlert] = useState("error")
    const closeAlert = () => {
        setDisplayAlert(false);
    }
    const [country, seCountry] = useState("Viet Nam")
    const [phone, setPhone] = useState(0)
    const [dataInp, setDataInp] = useState({
        fullName: "",
        email: "",
        address: "",
    })

    const handleChange = (event) => {
        setDataInp({
            ...dataInp,
            [event.target.name]: event.target.value
        })
    };
    const handleAddClick = () => {
        var validateResult = validateInp()
        if (validateResult) {
            var addData = { ...dataInp, phone, country, city }
            dispatch(addCusAction(addData, setDisplayAlert, settextAlert, setAlert, setVisible, varRefeshPage, setVarRefeshPage));
        }
    }
    const validateInp = () => {
        if (dataInp.fullName == "") {
            setDisplayAlert(true);
            settextAlert("please enter your full name")
            return false
        }
        if (!dataInp.email.match(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
            setDisplayAlert(true);
            settextAlert("email is invalid")
            return false
        }
        if (isNaN(phone) || phone <= 0) {
            setDisplayAlert(true);
            settextAlert("phone invalid")
            return false
        }
        if (!sltProvince) {
            setDisplayAlert(true);
            settextAlert("please select your province")
            return false
        }
        if (!city) {
            setDisplayAlert(true);
            settextAlert("please select your district")
            return false
        }
        if (dataInp.address == "") {
            setDisplayAlert(true);
            settextAlert("please enter your address")
            return false
        }

        if (dataInp.country == "") {
            setDisplayAlert(true);
            settextAlert("please enter your country")
            return false
        }
        return true
    }
    useEffect(() => {
        dispatch(getProvince())
    }, [])
    useEffect(() => {
        dispatch(getDistrict(sltProvince))
    }, [sltProvince])

    return (
        <>
            <Snackbar
                open={displayAlert}
                onClose={closeAlert}>
                <Stack sx={{ width: '100%' }} spacing={2} >
                    <Alert severity={typeAlert}>{textAlert} </Alert>
                </Stack>
            </Snackbar>

            <CModal visible={visible} onClose={() => setVisible(false)} >
                <CModalHeader onClose={() => setVisible(false)}>
                    <CModalTitle style={{ marginLeft: "120px" }}>ADD NEW CUSTOMER</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    <div style={{ textAlign: "center" }}>
                        <Box component="form" sx={{ '& > :not(style)': { m: 1 } }}  >
                            <FormControl focused variant="standard" sx={{ width: "300px" }}>
                                <InputLabel htmlFor="component-error">Name</InputLabel>
                                <Input onChange={handleChange} aria-describedby="component-error-text" name="fullName" />
                            </FormControl>
                        </Box>
                        <Box component="form" sx={{ '& > :not(style)': { m: 1 } }} >
                            <FormControl focused variant="standard" sx={{ width: "300px" }}>
                                <InputLabel htmlFor="component-error">Email</InputLabel>
                                <Input onChange={handleChange} aria-describedby="component-error-text" name="email" />
                            </FormControl>
                        </Box>
                        <Box sx={{ '& > :not(style)': { m: 1 } }}  >
                            <FormControl focused variant="standard" sx={{ width: "300px" }}>
                                <InputLabel htmlFor="standard-adornment-amount" >Phone</InputLabel>
                                <Input onChange={(e) => { setPhone(Number(e.target.value)) }} name="phone" />
                            </FormControl>
                        </Box>
                        <Box component="form" sx={{ '& > :not(style)': { m: 1 } }} >
                            <FormControl focused variant="standard" sx={{ width: "300px" }}>
                                <InputLabel htmlFor="component-error">Country</InputLabel>
                                <Input value={country} aria-describedby="component-error-text" disabled />
                            </FormControl>
                        </Box>
                        <div className="row mt-3" style={{ marginLeft: "60px" }}>
                            <div className="col-sm-5">
                                <Box >
                                    <FormControl >
                                        <InputLabel variant="standard" htmlFor="uncontrolled-native">
                                            Province
                                        </InputLabel>
                                        <NativeSelect sx={{ width: "120px" }}
                                            onChange={(event) => {
                                                setSltProvince(event.target.value);
                                            }}
                                        >
                                            <option value="0">All</option>
                                            {province ? province.map((item, index) => {
                                                return (
                                                    <option value={item.code} key={index}>{item.name}</option>
                                                )
                                            }) : null}
                                        </NativeSelect>
                                    </FormControl>
                                </Box>
                            </div>
                            <div className="col-sm-5">
                                <Box>
                                    <FormControl >
                                        <InputLabel variant="standard" htmlFor="uncontrolled-native">
                                            Districts
                                        </InputLabel>
                                        <NativeSelect sx={{ width: "120px" }}
                                            onChange={(event) => {
                                                setCity(event.target.value);
                                            }}
                                        >
                                            <option value="0">All</option>
                                            {district ? district.map((item, index) => {
                                                return (
                                                    <option value={district.name} key={index}>{item.name}</option>
                                                )
                                            }) : null}
                                        </NativeSelect>
                                    </FormControl>
                                </Box>
                            </div>
                        </div>
                        <Box component="form" sx={{ '& > :not(style)': { m: 1 } }} >
                            <FormControl focused variant="standard" sx={{ width: "300px" }}>
                                <InputLabel htmlFor="component-error">Address</InputLabel>
                                <Input onChange={handleChange} aria-describedby="component-error-text" name="address" />
                            </FormControl>
                        </Box>

                    </div>

                </CModalBody>
                <CModalFooter>
                    <CButton color="secondary" onClick={() => setVisible(false)}>
                        Close
                    </CButton>
                    <CButton color="primary" onClick={handleAddClick}>Save Customer</CButton>
                </CModalFooter>
            </CModal>
        </>
    )
}

export default AddCusModal;