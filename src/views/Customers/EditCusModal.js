import { CButton, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle } from "@coreui/react";
import { Alert, Box, FormControl, Input, InputLabel, Snackbar, Stack } from "@mui/material";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { editCusAction } from "src/actions/CustomersAction";

const EditCusModel = ({ dataEdit, setDataEdit, visible, setVisible, varRefeshPage, setVarRefeshPage }) => {
    var dispatch = useDispatch()
    //alert
    const [displayAlert, setDisplayAlert] = useState(false);
    const [textAlert, settextAlert] = useState("text");
    const [typeAlert, setAlert] = useState("error")
    const closeAlert = () => {
        setDisplayAlert(false);
    }

    //change input
    const handleChange = (event) => {
        setDataEdit({
            ...dataEdit,
            [event.target.name]: event.target.value
        })
    };

    //click edit
    const handleEditClick = () => {
        var validateResult = validateInp()
        if (validateResult) {
            var editData = {
                fullName: dataEdit.fullName,
                email: dataEdit.email,
                phone: Number(dataEdit.phone),
                address: dataEdit.address,
                city: dataEdit.city,
                country: dataEdit.country
            }
            dispatch(editCusAction(editData, dataEdit._id, setDisplayAlert, settextAlert, setAlert, setVisible, varRefeshPage, setVarRefeshPage));
        }
    }

    const validateInp = () => {
        if (dataEdit.fullName == "") {
            setDisplayAlert(true);
            settextAlert("please enter your full name")
            return false
        }
        if (!dataEdit.email.match(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
            setDisplayAlert(true);
            settextAlert("email is invalid")
            return false
        }
        if (isNaN(dataEdit.phone) || dataEdit.phone <= 0) {

            setDisplayAlert(true);
            settextAlert("phone invalid")
            return false
        }
        if (dataEdit.address == "") {
            setDisplayAlert(true);
            settextAlert("please enter your address")
            return false
        }
        if (dataEdit.city == "") {
            setDisplayAlert(true);
            settextAlert("please enter your city")
            return false
        }
        if (dataEdit.country == "") {
            setDisplayAlert(true);
            settextAlert("please enter your country")
            return false
        }
        return true
    }

    return (
        <>
            <Snackbar
                open={displayAlert}
                onClose={closeAlert}>
                <Stack sx={{ width: '100%' }} spacing={2} >
                    <Alert severity={typeAlert}>{textAlert} </Alert>
                </Stack>
            </Snackbar>

            <CModal visible={visible} onClose={() => setVisible(false)} >
                <CModalHeader onClose={() => setVisible(false)}>
                    <CModalTitle style={{ marginLeft: "120px" }}>EDIT DATA CUSTOMER</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    <div style={{ textAlign: "center" }}>
                        <Box component="form" sx={{ '& > :not(style)': { m: 1 } }}  >
                            <FormControl focused variant="standard" sx={{ width: "300px" }}>
                                <InputLabel htmlFor="component-error">Name</InputLabel>
                                <Input onChange={handleChange} aria-describedby="component-error-text" name="fullName" value={dataEdit.fullName} />
                            </FormControl>
                        </Box>
                        <Box component="form" sx={{ '& > :not(style)': { m: 1 } }} >
                            <FormControl focused variant="standard" sx={{ width: "300px" }}>
                                <InputLabel htmlFor="component-error">Email</InputLabel>
                                <Input onChange={handleChange} aria-describedby="component-error-text" name="email" value={dataEdit.email} />
                            </FormControl>
                        </Box>
                        <Box sx={{ '& > :not(style)': { m: 1 } }}  >
                            <FormControl focused variant="standard" sx={{ width: "300px" }}>
                                <InputLabel htmlFor="standard-adornment-amount" >Phone</InputLabel>
                                <Input onChange={handleChange} aria-describedby="component-error-text" name="phone" value={dataEdit.phone} />
                            </FormControl>
                        </Box>
                        <Box component="form" sx={{ '& > :not(style)': { m: 1 } }} >
                            <FormControl focused variant="standard" sx={{ width: "300px" }}>
                                <InputLabel htmlFor="component-error">Address</InputLabel>
                                <Input onChange={handleChange} aria-describedby="component-error-text" name="address" value={dataEdit.address} />
                            </FormControl>
                        </Box>
                        <Box component="form" sx={{ '& > :not(style)': { m: 1 } }} >
                            <FormControl focused variant="standard" sx={{ width: "300px" }}>
                                <InputLabel htmlFor="component-error">City</InputLabel>
                                <Input onChange={handleChange} aria-describedby="component-error-text" name="city" value={dataEdit.city} />
                            </FormControl>
                        </Box>
                        <Box component="form" sx={{ '& > :not(style)': { m: 1 } }} >
                            <FormControl focused variant="standard" sx={{ width: "300px" }}>
                                <InputLabel htmlFor="component-error">Country</InputLabel>
                                <Input onChange={handleChange} aria-describedby="component-error-text" name="country" value={dataEdit.country} />
                            </FormControl>
                        </Box>
                    </div>

                </CModalBody>
                <CModalFooter>
                    <CButton color="secondary" onClick={() => setVisible(false)}>
                        Close
                    </CButton>
                    <CButton color="primary" onClick={handleEditClick}>Update Customer</CButton>
                </CModalFooter>
            </CModal>
        </>
    )
}

export default EditCusModel; 