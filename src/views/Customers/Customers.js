import { CButton, CFormInput, CModal, CModalFooter, CModalHeader, CModalTitle, CTable, CTableBody, CTableCaption, CTableDataCell, CTableHead, CTableHeaderCell, CTableRow } from "@coreui/react";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { Alert, Grid, Pagination, Snackbar, Stack } from "@mui/material"
import { ChangeNoPagecustomer, customerAction, deleteCusAction } from "src/actions/CustomersAction";
import AddCusModal from "./AddCusModal";
import EditCusModel from "./EditCusModal";

const Customer = () => {
    const dispatch = useDispatch()
    const [varRefeshPage, setVarRefeshPage] = useState(0);
    const [dataEdit, setDataEdit] = useState("")
    const [idDelete, setIdDelete] = useState("")

    const { currentPage_customer, customer, noPage_customer } = useSelector((reduxData) => reduxData.customerReducer);
    const limit = 10;
    const [filterName, setFilterName] = useState("")
    const [visibleDelete, setVisibleDelete] = useState(false);
    const [visibleEdit, setVisibleEdit] = useState(false);
    const [visibleAdd, setVisibleAdd] = useState(false);

    const changePageHandler = (event, value) => {
        dispatch(ChangeNoPagecustomer(value))
    }
    //alert
    const [displayAlert, setDisplayAlert] = useState(false);
    const [textAlert, settextAlert] = useState("text");
    const [typeAlert, setAlert] = useState("error")
    const closeAlert = () => {
        setDisplayAlert(false);
    }
    //handleAdd
    const handleAddProduct = () => {
        return setVisibleAdd(!visibleAdd)
    }
    //handleEdit
    const handleEdit = (dataItem) => {
        setDataEdit(dataItem)
        return setVisibleEdit(!visibleEdit);

    }
    const handleDelete = (id) => {
        setIdDelete(id)
        return setVisibleDelete(!visibleDelete);
    }
    const handleDeleteClick = () => {
        dispatch(deleteCusAction(idDelete, setDisplayAlert, settextAlert, setAlert, setVisibleDelete, varRefeshPage, setVarRefeshPage))
    }


    useEffect(() => {
        dispatch(customerAction(filterName, limit, currentPage_customer))
    }, [currentPage_customer, varRefeshPage, filterName])

    return (
        <>
            <AddCusModal
                visible={visibleAdd}
                setVisible={setVisibleAdd}
                varRefeshPage={varRefeshPage}
                setVarRefeshPage={setVarRefeshPage}
            />
            <EditCusModel
                dataEdit={dataEdit}
                setDataEdit={setDataEdit}
                visible={visibleEdit}
                setVisible={setVisibleEdit}
                varRefeshPage={varRefeshPage}
                setVarRefeshPage={setVarRefeshPage}
            />
            <Snackbar
                open={displayAlert}
                onClose={closeAlert}>
                <Stack sx={{ width: '100%' }} spacing={2} >
                    <Alert severity={typeAlert}>{textAlert} </Alert>
                </Stack>
            </Snackbar>

            <CModal visible={visibleDelete} onClose={() => setVisibleDelete(false)} >
                <CModalHeader onClose={() => setVisibleDelete(false)}>
                    <CModalTitle style={{ marginLeft: "120px" }}>DELETE THIS CUSTOMER ? </CModalTitle>
                </CModalHeader>
                <CModalFooter>
                    <CButton color="primary" onClick={handleDeleteClick} className=" mx-auto">Delete Product</CButton>
                </CModalFooter>
            </CModal>
            <h1>All CustomersAction</h1>
            <CTable striped hover borderless caption="top" responsive>
                <CTableCaption >
                    <div className="row">
                        <div className="col-sm-10">
                            <img src="https://img.icons8.com/plasticine/50/000000/plus-2-math.png" style={{ cursor: "pointer" }} onClick={handleAddProduct} /> Add new
                        </div>
                        <div className="col-sm-2">
                            <CFormInput placeholder='search by name' style={{ width: "100%" }} onChange={e => { setFilterName(e.target.value) }} />
                        </div>
                    </div>
                </CTableCaption>
                <CTableHead color="dark">
                    <CTableRow>
                        <CTableHeaderCell scope="col">STT</CTableHeaderCell>
                        <CTableHeaderCell scope="col">Full Name</CTableHeaderCell>
                        <CTableHeaderCell scope="col">Phone</CTableHeaderCell>
                        <CTableHeaderCell scope="col">Email</CTableHeaderCell>
                        <CTableHeaderCell scope="col">Address</CTableHeaderCell>
                        <CTableHeaderCell scope="col">City</CTableHeaderCell>
                        <CTableHeaderCell scope="col">Country</CTableHeaderCell>
                        <CTableHeaderCell scope="col">Order</CTableHeaderCell>
                        <CTableHeaderCell scope="col">Action</CTableHeaderCell>
                    </CTableRow>
                </CTableHead>
                <CTableBody>
                    {customer ? customer.map((item, index) => {
                        return (
                            <CTableRow key={index}>
                                <CTableHeaderCell scope="row">{index + 1}</CTableHeaderCell>
                                <CTableDataCell>{item.fullName}</CTableDataCell>
                                <CTableDataCell>{item.phone}</CTableDataCell>
                                <CTableDataCell>{item.email}</CTableDataCell>
                                <CTableDataCell>{item.address}</CTableDataCell>
                                <CTableDataCell>{item.city}</CTableDataCell>
                                <CTableDataCell >{item.country}</CTableDataCell>
                                {item.orders.length > 0 ? <CTableDataCell>{item.orders.map((ordersItem, index) => {
                                    return (
                                        <div key={index}>
                                            <p >
                                                {ordersItem._id}
                                            </p>
                                        </div>
                                    )
                                })}</CTableDataCell> : <CTableDataCell></CTableDataCell>}
                                <CTableDataCell ><img src="https://img.icons8.com/stickers/24/000000/pencil.png" style={{ cursor: "pointer" }} onClick={() => handleEdit(item)} />&emsp;<img src="https://img.icons8.com/external-anggara-filled-outline-anggara-putra/24/000000/external-delete-ecommerce-anggara-filled-outline-anggara-putra.png" style={{ cursor: "pointer" }} onClick={() => handleDelete(item._id)} /></CTableDataCell>
                            </CTableRow>
                        )
                    }) : null
                    }
                </CTableBody>
            </CTable>
            <Grid container mt={3} justifyContent="flex-end">
                <Grid item>
                    <Pagination count={noPage_customer} defaultPage={1} onChange={changePageHandler} color="primary" variant="outlined" shape="rounded" />
                </Grid>
            </Grid>
        </>
    )
}
export default Customer; 